<div class="row mt-4">
    <div class="col">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                        <th>Office Transactions</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><a href="/add">+ Add Transactions</a></th>
                    </tr>
                   
                    <tr>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Credit</th>
                        <th>Debit</th>
                        <th>Running balances</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($transactions as $transaction)
                    <tr>
                        <td>{{$transaction->created_at->toDateString()}}</td>
                        <td>{{$transaction->Description ?? 'Null'}}</td>
                        <td>{{$transaction->Credit ?? 'Null'}}</td>
                        <td>{{$transaction->Debit ?? 'Null'}}</td>
                        <td>{{$transaction->Balance}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div><!--col-->
</div><!--row-->